var errors={

}
var form_data=[]
const getTotal=(classe)=>{
    let total=0;
    form_data.map((item)=>{
        if(item.classe===classe){
            return total+=item.value
        }
    });
    return total
  }
const getErros=(item)=>{
    
    return getTotal(item.classe)<parseInt(item.minimum)?`La quantite minimal n'est pas respecte pour la famille de  ${item.classe} qui est de ${item.minimum}`:``
}
const handleChange=(classe,produit, value, minimum)=>{

    form_data=[...form_data.filter(obj=>obj.classe!==classe || obj.produit!==produit), {
      classe:classe,
      produit:produit,
      value:isNaN(parseInt(value))?0:parseInt(value),
      minimum:isNaN(parseInt(minimum))?0:parseInt(minimum)
    }]
    //$(`#${classe}`).html(getErros({classe:classe, produit:produit, minimum:parseInt(minimum)}))
    console.log(form_data)    
}

const handleSubmit=()=>{
    let error=false;
    const data=JSON.parse(localStorage.getItem('data'));
    data.map((item)=>{
     if(getTotal(item.classe)<parseInt(item.minimum)){
      error=true
     }
     $(`#${item.classe}`).html(getErros(item))
    })
    if(error){
        throw console.log(error)
    }else{
      /*
      Il faut mettre le code de soumission ici.
      */
     console.log("j'attend le code de soumission!!!!!!!")
    }
   }
const renderForm=(data)=>{
    let section="" // Permet de generer une section, une classe
    // cette boucle permet de parcourir toutes les classes
	 
			
    data.map((item,Key)=>{
        let sect_1=`<div class='row section'> 
                        <b class='col-lg-12'>${item.classe}</b>
                        <div class="col-md-12">
						<table>
    <tbody>`;
        //Cette boucle permet de parcourir les produit de la classe courante
        item.produits.map((elt,key)=>{
            //Ici nous construisons pour chaque produit un champs et la fonction handleChange nous permet 
            //de detecter la saisie dans les champs
            sect_1+=`
			<tr>
            <td> Nom produit : ${elt.nom} </td>
            <td> prix <input type="text" name="prix[]"  value=${elt.prix} id=prix class="form-control"  ></td>
            <td> <input type=${item.minimum} name=${item.classe} onkeyup="handleChange(this.name,this.id,this.value,this.type)" id=${elt.nom} class="form-control" placeholder="+" aria-describedby="helpId"></td>
            <td> <input type= name= value="" readonly class="form-control" placeholder=""></td>
             </tr>
            
            <small id="helpId" class="text-muted"></small>
          </div>
            `
        })
        sect_1+=`</tbody></table>
        <small id=${item.classe} style="color:red" class="text-muted"></small>
        </div>
        </div>
        `;
        section+=sect_1;
    })
    section+=`
    <button onclick="handleSubmit()" type="button" value="send" class="btn btn-primary">Submit</button>
    `
    $("#form").html(section);
    console.log(JSON.parse(localStorage.getItem('data')))
}
$( document ).ready(function() {
    $("#form").html(`<center><img src="spinner.gif"  alt="loader"></center>`);
    localStorage.removeItem('data');
    $(function() { // ready
        $.getJSON('dada.php', function(data) {
            let donnees =[];
            let is_selected=[]
          $.each(data, function(key, val) { // pour chaque entrée du tableau
           if(is_selected.filter(obj=>obj===val.categories_id).length===0){
                var produits=[];
                data.filter(obj=>obj.categories_id===val.categories_id).map((item)=>{
                    produits.push(
                        {
                            id: item.product_id,
                            nom:item.quantity,
                            prix:item.rate, 
                            value:item.quantity  
                        }
                    )
                });
                donnees.push(
                    { 
                        id: val.categories_id,
                        classe:val.categories_name,
                        minimum: val.QteCheck,
                        produits:produits
                    }
                );
                is_selected.push(val.categories_id)
           }
           });
           localStorage.setItem('data',JSON.stringify(donnees))
           setTimeout(
               ()=>{
                   renderForm(donnees)
               },1500
           )
           console.log("########",donnees)   
         });
         
        });
});